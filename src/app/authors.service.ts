import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {
  authors:any =  [{id:1, author:'Saphon'},{id:2, author:'Leo Tolstoy'}]
id : number= 2;
  addAuthors(authornew:string){
    this.id=this.id+1;  
    this.authors.push({id:this.id,author:authornew});
  }
  getAuthors(): any {
    const authorsObservable = new Observable(observer => {
           setInterval(() => 
               observer.next(this.authors)
           , 4000);
    });

    return authorsObservable;
}
  constructor() { }
}
