 import { PostsService } from './../posts.service';
 import { Component, OnInit } from '@angular/core';
 import { Router, ActivatedRoute } from '@angular/router';
 
 @Component({
   selector: 'app-add-post',
   templateUrl: './add-post.component.html',
   styleUrls: ['./add-post.component.css']
 })
 export class AddPostComponent implements OnInit {
 
   author:string;
   name:string;
   body:string;
   id:string;
   isedit:boolean = false;
   text:string = "Add book";
 
   constructor(private postservice:PostsService,private router: Router,private route:ActivatedRoute) { }
 
   onSubmit(){
     if (this.id) {
       this.postservice.updatePost(this.id,this.name,this.author,this.body);
     }else{
       this.postservice.addPosts(this.body,this.name,this.author);
     }
     this.router.navigate(['/Posts']);
   }
 
   ngOnInit() {
       this.id = this.route.snapshot.params.id;
       
       if (this.id) {
         this.isedit = true; 
         this.text = "Updaate book"
         this.postservice.getpost(this.id).subscribe(
           post=>{
             this.name = post.data().name;
             this.author = post.data().author;
             this.body = post.data().body;
           }
         )
       }
   }
 
 }