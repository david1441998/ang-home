import { User } from './../interface/user';
import { PostsService } from './../posts.service';
import { Post } from './../interface/post';
import { Component, OnInit } from '@angular/core';
import 'firebase/firestore';
import { observable, Observable } from 'rxjs';      
@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  panelOpenState = false;
 
  posts:any;
    posts$:Observable<any>;

  constructor(private postsrvice: PostsService) { }

  /*myFunc(){
    for (let index = 0; index < this.Posts$.length; index++) {
      for (let i = 0; i < this.User$.length; i++) {
        if (this.Posts$[index].userId==this.User$[i].id) {
          this.title = this.Posts$[index].title;
          this.body = this.Posts$[index].body;
          this.author = this.User$[i].name;
          this.postsrvice.addPosts(this.body,this.author,this.title);
          
        }
        
        
      }
      
    }
    this.ans ="The data retention was successful"
  }*/
  deletepost(id:string){
     this.postsrvice.deletePost(id);
   }

  ngOnInit() {/*
    this.postsrvice.getUser()
    .subscribe(data =>this.User$ = data );
    this.postsrvice.getPost()
    .subscribe(data =>this.Posts$ = data );
    */
    this.posts$ = this.postsrvice.getposts();
   
  }

}
