import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthorsService } from './../authors.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private authorservice:AuthorsService) { }
  authors$:Observable<any>;
  author; 
  id;
  newauthor:string;
  authors:any;
  
  add(){
        this.authorservice.addAuthors(this.newauthor);
      }


  ngOnInit() {
    this.author = this.route.snapshot.params.author;
    this.id = this.route.snapshot.params.id;
    this.authors$ = this.authorservice.getAuthors();
  }


}
   
  

   
 
 