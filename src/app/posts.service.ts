 import { HttpClientModule, HttpClient } from '@angular/common/http';
 import { Injectable } from '@angular/core';
 import { Post } from './interface/post';
 import { User } from './interface/user';
 import { AngularFirestore } from '@angular/fire/firestore';
 import { Observable } from 'rxjs';
 @Injectable({
   providedIn: 'root'
 })
 export class PostsService {
 
   apiurl = "https://jsonplaceholder.typicode.com/posts"
   apiurl1 =  "https://jsonplaceholder.typicode.com/users"

   getposts():Observable<any[]>{
         return this.db.collection('posts').valueChanges({idField:'id'});
       }
     
   addPosts(body:string, author:string,name:string){
         const post = {body:body, author:author, name:name};
         this.db.collection('posts').add(post);
       }
   constructor(private _http: HttpClient,private db:AngularFirestore  ) { }
 
   getpost(id:string):Observable<any>{
             return this.db.doc(`posts/${id}`).get();
       }
       updatePost(id:string,name:string,author:string,body:string){
             const post = {name:name,author:author,body:body};
             this.db.doc(`posts/${id}`).update(post);
           }
           deletePost(id:string){
                this.db.doc(`posts/${id}`).delete();
               }
   getUser(){
    return this._http.get<User[]>(this.apiurl1);
  }
 }