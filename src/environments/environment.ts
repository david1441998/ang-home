// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
   firebaseConfig :{
    apiKey: "AIzaSyDxZfraF-eeg2UzAqsW0AvIPcJrnxrS5xw",
    authDomain: "home-5d140.firebaseapp.com",
    databaseURL: "https://home-5d140.firebaseio.com",
    projectId: "home-5d140",
    storageBucket: "home-5d140.appspot.com",
    messagingSenderId: "319834591499"
}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
